CREATE TABLE player (
  player_id INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  login VARCHAR(20) NOT NULL,
  passwd VARCHAR(50) NOT NULL,
  created_date DATETIME NULL,
  last_update TIMESTAMP NULL DEFAULT NULL,
  last_login DATETIME NULL DEFAULT NULL,
  email VARCHAR(50) NULL DEFAULT NULL,
  gsm VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY(player_id),
  UNIQUE INDEX UQ_login(login)
)
TYPE=InnoDB;

CREATE TABLE game (
  game_id INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  game_date DATETIME NULL,
  PRIMARY KEY(game_id)
)
TYPE=InnoDB;

CREATE TABLE player_in_game (
  player_id INTEGER(11) UNSIGNED NOT NULL,
  game_id INTEGER(11) UNSIGNED NOT NULL,
  PRIMARY KEY(player_id, game_id),
  INDEX player_in_game_FKIndex1(player_id),
  INDEX player_in_game_FKIndex2(game_id),
  FOREIGN KEY(player_id)
    REFERENCES player(player_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(game_id)
    REFERENCES game(game_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
)
TYPE=InnoDB;


