<?php
require_once APPLICATION_PATH . '/models/Player.php';
require_once APPLICATION_PATH . '/models/DbTable/Player.php';


class PlayerList
{
    
    protected $_list;
    protected $_table;

    public function __construct($input = array()) 
    {
	$this->_list = new ArrayObject($input);
    }

    public function getPlayerByID($player_id)
    {
        return $this->_list->offsetGet($player_id);
    }

    public function addPlayer($player) 
    {
        //@todo - mal by vyhodit exception ked tam uz bude dane player id
        if (!$this->exists($player->getID())) {
            $this->_list->offsetSet($player->getID(), $player);
        }
	return $this;
    }

    public function removePlayer($player)
    {
        if ($this->exists($player->getID())) {
            $this->_list->offsetUnset($player->getID());
        }
	return $this;
    }

    public function addPlayers($players)
    {
	foreach ($players as $player) {
		$this->_addPlayer($player);
	}	
	return $this;
    }

    public function fetchAllPlayers() {
        $table = $this->getTable();
        $rowset = $table->fetchAll();
        foreach ($rowset as $row) {
            $player = new Player($row->player_id);
            $this->addPlayer($player);
        }

        return $this;
    }

    public function exists($player_id) {
	if ($this->_list->offsetExists($player_id)) {
            return true;
        }
        else {
            return false;
        }
    }

    public function count() 
    {
	return $this->_list->count();
    }

    public function getIterator()
    {
	return $this->_list->getIterator();
    }
    
    /**
    * Retrieve table object
    *
    * @return Model_Guestbook_Table
    */
    protected function getTable()
    {
        if (null === $this->_table) {
            $this->_table = new Model_DbTable_Player();
        }
        return $this->_table;
    }
}
?>
