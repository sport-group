<?php
class Player
{
    const PLAYER_ID = 'player_id';
    
    protected $_table;
    protected $_player_id = null;
    protected $_info;


    // @todo bude sa slectovat cez id alebo cez login?
    public function __construct($player_id = null) 
    {
	if (null !== $player_id) {
	    //ziskat vsetky data o uzivatelovi
	    $this->_player_id = $player_id;
	    $table = $this->getTable();
	    $select = $table->select()->where(self::PLAYER_ID.'=?', $player_id);
	    $this->_info = $table->fetchRow($select);  //->toArray();
	}
    }

    public function getID()
    {
	return $this->_player_id;
    }

    public function getInfo($info_offset = '') 
    {
    	if (empty($info_offset)) {
	    return $this->_info;
	}
	else {
	    return $this->_info[$info_offset]; 
	}
    }

    public function changeInfo($offset, $value)
    {
	$this->_info[$offset] = $value;
	$this->getTable()->update(array($offset,$value), 
					$this->PLAYER_ID.' = '.$this->_player_id);
	return $this;
    }

    /**
    * Retrieve table object
    *
    * @return Model_Guestbook_Table
    */
    protected function getTable()
    {
	if (null === $this->_table) {
	    // since the dbTable is not a library item but an application item,
	    // we must require it to use it
	    require_once APPLICATION_PATH . '/models/DbTable/Player.php';
	    $this->_table = new Model_DbTable_Player();
	}
	return $this->_table;
    }

    /**
     * Save a new entry
     * 
     * @param  array $data 
     * @return int|string
     */
    protected function save(array $data)
    {
        $table  = $this->getTable();
	$fields = $table->info(Zend_Db_Table_Abstract::COLS);
	$player_id = $this->getID();
        
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
	}
	if (null !== $player_id) {
	    $table->update($data, $this->PLAYER_ID.' = '.$player_id);
	    return $player_id;
	}
	else {
	    return $table->insert($data);
	}
    }
}
?>
