<?php
require_once APPLICATION_PATH.'/models/Game.php';
require_once APPLICATION_PATH.'/models/Player.php';
require_once APPLICATION_PATH.'/models/PlayerList.php';
require_once APPLICATION_PATH.'/models/DbTable/Game.php';
class GameManager {
    	
    const GAME_DATE = 'game_date';
    const GAME_ID = 'game_id';
    
    protected $_game_list = array();
    protected $_table;


    //@todo prepracovat triedu tak aby si spravovala zoznam hier.
    //podla toho co sa zada v konstruktore sa kedtak obmedzi mnozstvo 
    //uschovavanych dat. Defaultne sa nacita kompletny zoznam
    public function __construct($start_date = null, $end_date = null)
    {
	$select = $this->getTable()->select()->order(array(self::GAME_DATE.' DESC'));
	if (null !== $start_date) {
	    $select->where(self::GAME_DATE.'>?', $start_date);
	}

	if (null !== $end_date) {
	    $select->where(self::GAME_DATE.'<?', $end_date);
	}

	$res = $select->query();

	if ($games=$res->fetchAll()) {
	    foreach ($games as $row) {
		$this->_game_list[] = new Game($row[self::GAME_ID]);
	    }
	}
	else {
	    $this->_game_list[] = $this->createNewGame();
	}

	$actual_game = $this->_game_list[0];
	$game_date = new Zend_Date($actual_game->getDate());
	if ( -1 == $game_date->compare(Zend_Date::now())) {
	    $this->_game_list = array_unshift($this->_game_list, 
		    				$this->createNewGame());
	}

    }
    
    public function getActualGameID() 
    {
	return $this->getActualGame()->getID();
    }

    public function getActualGameDate() 
    {
	return $this->getActualGame()->getDate();
    }

    public function createNewGame()
    {
	$game_date = $this->_getNewGameDate();
	$game_table = new Model_DbTable_Game();
	$game_id = $game_table->insert(array('game_date'=>$game_date->toString('Y-m-j H:i:s')));
	return new Game($game_id);

    }

    //@todo dodelat
    public function getActualGame() 
    {
	return $this->_game_list[0];
    }

    //@todo dodelat
    public function getGameByDate()
    {
    }

    // @todo dodelat
    public function getGames()
    {
	return $this->_game_list;
    }

    //@todo dodelat
    public function getGameByID()
    {
    }

    public function signPlayer($player_id) 
    {
	$player = new Player($player_id);
	$actual_game = $this->getActualGame();
	$actual_game->addPlayer($player);
    }

    public function unsignPlayer($player_id)
    {	
	$player = new Player($player_id);
	$actual_game = $this->getActualGame();
	$actual_game->removePlayer($player);
    }


    protected function _getNewGameDate()
    {
	$game_date = new Zend_Date();
	$game_date->setOptions(array('format_type'=>'php'));
	// nastavi utorok
	$game_date->setWeekday(2);
	//nastavi cas 18:00:00
	$game_date->setHour(18);
	$game_date->setMinute(0);
	$game_date->setSecond(0);
	if (-1 == $game_date->compare(Zend_Date::now())) {
		$game_date->addWeek(1);
	}
	return $game_date;
    }
    
    /**
    * Retrieve table object
    *
    * @return Model_Guestbook_Table
    */
    protected function getTable()
    {
	if (null === $this->_table) {
	    // since the dbTable is not a library item but an application item,
	    // we must require it to use it
	    $this->_table = new Model_DbTable_Game();
	}
	return $this->_table;
    }

}
?>
