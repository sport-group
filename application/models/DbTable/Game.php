<?php
class Model_DbTable_Game extends Zend_Db_Table_Abstract
{
	/** Table name */
	protected $_name    = 'game';

	protected $_dependentTables = array('player_in_game');
}