<?php
class Model_DbTable_Player extends Zend_Db_Table_Abstract
{
	/** Table name */
	protected $_name    = 'player';

	/**
	* Override updating
	*
	* Do not allow updating of entries
	*
	* @param  array $data
	* @param  mixed $where
	* @return void
	* @throws Exception
	*/
	public function update(array $data)
	{
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
        
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }
        
		$data['last_update'] = time();
		return parent::update($data);
	}
	
    /**
	* Insert new row
	*
	* Ensure that a timestamp is set for the created field.
	*
	* @param  array $data
	* @return int
	*/
	public function insert(array $data)
	{
		$data['created_date'] = date('Y-m-d H:i:s');
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
        
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }
        
		return parent::insert($data);
	}


}

class DbTable_Player extends Zend_Db_Table_Abstract
{
	/** Table name */
	protected $_name    = 'player';

	/**
	* Override updating
	*
	* Do not allow updating of entries
	*
	* @param  array $data
	* @param  mixed $where
	* @return void
	* @throws Exception
	*/
	public function update(array $data)
	{
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
        
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }
        
		$data['last_update'] = time();
		return parent::update($data);
	}
	
    /**
	* Insert new row
	*
	* Ensure that a timestamp is set for the created field.
	*
	* @param  array $data
	* @return int
	*/
	public function insert(array $data)
	{
		$data['created_date'] = date('Y-m-d H:i:s');
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
        
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }
        
		return parent::insert($data);
	}


}
