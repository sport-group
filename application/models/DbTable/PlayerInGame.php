<?php
class Model_DbTable_PlayerInGame extends Zend_Db_Table_Abstract
{
	/** Table name */
	protected $_name    = 'player_in_game';

	protected $_referenceMap = array(
		'Player' => array(
			'columns'=>'player_id'
			,'refTableClass'=>'Model_DbTable_Player'
			,'refColumns'=>'player_id'
			,'onDelete'=>self::CASCADE
			,'onUpdate'=>self::CASCADE
		),
		'Game' => array(
			'columns'=>'game_id'
			,'refTableClass'=>'Model_DbTable_Game'
			,'refColumns'=>'game_id'
			,'onDelete'=>self::CASCADE
			,'onUpdate'=>self::CASCADE
		)
	);
}


class DbTable_PlayerInGame extends Zend_Db_Table_Abstract
{
	/** Table name */
	protected $_name    = 'player_in_game';

	protected $_referenceMap = array(
		'Player' => array(
			'columns'=>'player_id'
			,'refTableClass'=>'DbTable_Player'
			,'refColumns'=>'player_id'
			,'onDelete'=>self::CASCADE
			,'onUpdate'=>self::CASCADE
		),
		'Game' => array(
			'columns'=>'game_id'
			,'refTableClass'=>'Model_DbTable_Game'
			,'refColumns'=>'game_id'
			,'onDelete'=>self::CASCADE
			,'onUpdate'=>self::CASCADE
		)
	);
}
