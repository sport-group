<?php
require_once APPLICATION_PATH . '/models/Player.php';
require_once APPLICATION_PATH . '/models/PlayerList.php';
require_once APPLICATION_PATH . '/models/DbTable/Game.php';
require_once APPLICATION_PATH . '/models/DbTable/Player.php';
require_once APPLICATION_PATH . '/models/DbTable/PlayerInGame.php';

class Game
{
	const TABLE_GAME = 'game';
	const GAME_DATE = 'game_date'; 
	const GAME_ID = 'game_id';
	const PLAYER_ID = 'player_id';	

	protected $_table;
	protected $_game_date;
	protected $_game_id;
	protected $_playerlist;

	public function __construct($game_id)
	{
	    $game_table = $this->getTable();

	    $select = $game_table->select()->where(self::GAME_ID.'=?', $game_id);
	    $stmt = $select->query();

	    if ($res = $stmt->fetchAll()) {
		$this->_game_id = $game_id;
		$this->_game_date = $res[0][self::GAME_DATE];
	    }
	    else {
		throw Exeption('Neplatne game_id');
	    }
	}
	

	public function getPlayerList() 
	{
	    if (null == $this->_playerlist) {
		$this->_playerlist = new PlayerList();
    
		$game_table = $this->getTable();
		$games_rowset = $game_table->find($this->getID());
		$act_game = $games_rowset->current();
		$players_rowset = $act_game->findManyToManyRowset('DbTable_Player', 'DbTable_PlayerInGame');
		foreach ($players_rowset as $row) {
		    $player = new Player($row->player_id);
		    $this->_playerlist->addPlayer($player);
		}	
	    }
	    return $this->_playerlist;
	}

	public function addPlayer($player)
	{
	    $this->getPlayerList()->addPlayer($player);
	    // ulozi zaznam do DB
	    $player_in_game_table = new Model_DbTable_PlayerInGame();
	    $player_in_game_table->insert(array(self::PLAYER_ID=>$player->getID(),
						self::GAME_ID=>$this->getID()
					));
	    return $this;
	}

        public function removePlayer($player)
	{
            $this->getPlayerList()->removePlayer($player);
	    // zmaze zaznam v DB
	    $player_in_game_table = new Model_DbTable_PlayerInGame();
            $where1 = $player_in_game_table->getAdapter()->quoteInto(self::PLAYER_ID.'= ? '
                                                                    ,$player->getID());
            $where2 = $player_in_game_table->getAdapter()->quoteInto(self::GAME_ID.'= ? '
                                                                    ,$this->getID());
            $player_in_game_table->delete(array($where1, $where2));
	    return $this;
	}

	public function getDate()
	{
	    return $this->_game_date;
	}

	public function getID()
	{
	    return $this->_game_id;
	}

	public function countPlayers() 
	{
	    return $this->getPlayerList()->count();
	}

	public function signPlayer($palyer) {
	}



	protected function _getNewGameDate()
	{
	    $game_date = new Zend_Date();
	    $game_date->setOptions(array('format_type'=>'php'));
	    // nastavi utorok
	    $game_date->setWeekday(2);
	    //nastavi cas 18:00:00
	    $game_date->setHour(18);
	    $game_date->setMinute(0);
	    $game_date->setSecond(0);
	    if (-1 == $game_date->compare(Zend_Date::now())) {
		    $game_date->addWeek(1);
	    }
	    return $game_date;
	}
	
	protected function _createNewGame()
	{
		$game_date = $this->_getNewGameDate();
		$game_id = $this->getTable()->insert(array('game_date'=>$game_date->toString('Y-m-j H:i:s')));
		return array($game_id=>$game_date);
	}
	
	protected function getTable()
	{
	    if (null === $this->_table) {
		// since the dbTable is not a library item but an application item,
		// we must require it to use it
		$this->_table = new Model_DbTable_Game();
	    }
	    return $this->_table;
	}


}
