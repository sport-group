<?php
class Model_PlayerInGame
{
	protected $_table;

	public function getTable()
	{
		if (null === $this->_table) {
            // since the dbTable is not a library item but an application item,
            // we must require it to use it
			require_once APPLICATION_PATH . '/models/DbTable/PlayerInGame.php';
			$this->_table = new Model_DbTable_PlayerInGame();
		}
		return $this->_table;
	}
	
	/**
	* vrati zoznam hracov pre nastaveny datum
	*/
	public function getPlayerList()
	{
		$q = 'SELECT player_id, login, email, gsm
				FROM player AS p
					LEFT JOIN player_in_game AS pig
						ON p.player_id = pig.player_id
					LEFT JOIN game AS g
						ON g.game_id=pim.game_id
				WHERE 1';

		//tabulka game
		$game_table = new Model_DbTable_Game();
		//tabulka player
		$player_table = new Model_DbTable_Player();
		//vybrat hracov na najnovsiu hru
		$plaers = $player_table->fetchAll();
	}

}