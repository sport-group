<?php
// @todo nacitanie suborov a tried prepracovat pomocou Zend_Loader
require_once APPLICATION_PATH.'/models/Game.php';
require_once APPLICATION_PATH.'/models/GameManager.php';
require_once APPLICATION_PATH.'/models/Player.php';
require_once APPLICATION_PATH.'/models/PlayerList.php';

class IndexController extends Zend_Controller_Action
{
    const PLAYER_ID = 'player_id';
    protected $_model;
	
	public function indexAction()
	{
	    $this->view->actual_game_date = $this->_getModel()->getActualGameDate();
	    $this->view->player_list = $this->getActualPlayerlist();
	    $this->view->form = $this->_getSignForm();
	}

	public function signAction()
	{
	    //overi autorizaciu
	    $auth = $this->_getAuth();
	    
	    if ($auth->hasIdentity()) {
		//zaznamena ucast hraca v DB
		$storage = $auth->getStorage()->read();
		$this->_signPlayer($storage->player_id);
		//aktualizovany view so zoznamom hracov
		//@todo
	    }
	}

    public function unsignAction()
    {
        //overi autorizaciu
    $auth = $this->_getAuth();

    if ($auth->hasIdentity()) {
            $storage = $auth->getStorage()->read();
	//odstrani hraca z player_in_game
            $this->_unsignPlayer($storage->player_id);
    }
    }

	/*
	 * metoda zisti informacie o hracoch prihlasenych na aktualnu hru
	 * @todo tuto informaciu by mal poskytovat model - bude
	 * treba zapracovat na presune metody do modelu 
	 */
	public function getActualPlayerlist()
	{
	    return $this->_getModel()->getActualGame()->getPlayerList();
	}

	protected function _getModel()
	{
	    if (null === $this->_model) {			
		$this->_model = new GameManager();
	    }
	    return $this->_model;
	}

	protected function _getSignForm()
        {
	    $form = new Zend_Form();
	    $form->setAction($this->_helper->url('sign'))
		    ->setMethod('post');
	    $form->addElement('submit', 'prihlasit');
	    return $form;
	}

	protected function _signPlayer($player_id)
        {
	    $_model = $this->_getModel();
	    try {
                $_model->signPlayer($player_id);
            }catch (Zend_Db_Statement_Exception $exception) {
                echo $exception->getCode();
                echo $exception->getMessage();
            }
	}

    protected function _unsignPlayer($player_id)
    {
		$_model = $this->_getModel();
		try {
		    $_model->unsignPlayer($player_id);
		}catch (Zend_Db_Statement_Exception $exception) {
		    echo $exception->getCode();
		    echo $exception->getMessage();
		}
	}

    protected function _getAuth()
    {
        //overi autorizaciu
		$auth = Zend_Auth::getInstance();
		#$authAdapter = Zend_Registry::get('authAdapter');
		$authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Registry::get('dbAdapter'));
		$authAdapter->setTableName('player')
				->setIdentityColumn('login')
				->setCredentialColumn('passwd');
        return $auth;
    }
}
