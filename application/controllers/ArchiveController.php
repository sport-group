<?php
require_once APPLICATION_PATH.'/models/GameManager.php';

class ArchiveController  extends Zend_Controller_Action

{
    protected $_model;
    
    public function indexAction() 
    {
	$this->view->games = $this->_getModel()->getGames();
    }
    
    protected function _getModel()
    {
	if (null === $this->_model) {
	    // @todo nacitanie suborov a tried prepracovat pomocou Zend_Loader
	    $this->_model = new GameManager();
	}
	return $this->_model;
    }

}
