<?php
class MenuController extends Zend_Controller_Action
{

    public function init()
    {
	$this->render();
    }

    public function applicationAction()
    {
        // we don't want to append the menu to the end
        // of the layout content, so:
        $this->_helper->viewRenderer->setResponseSegment('menu');
        $this->view->menu = array('x', 'y', 'z');
    }
    
    public function anotherAction()
    {
        $this->_helper->viewRenderer->setResponseSegment('menu');
        $this->view->menu = array('a', 'b', 'b');
    }
}
