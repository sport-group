<?php
require_once APPLICATION_PATH.'/models/DbTable/Player.php';
require_once APPLICATION_PATH.'/models/Player.php';
class UserController extends Zend_Controller_Action
{

    protected $_form;
    protected $_model;
    protected $_table;

    public function indexAction()
    {
		//@todo inicializacia formu ma byt na jednom mieste
		$this->_form =  $this->_getLoginForm();
		$this->view->form = $this->_form;
		$this->render('loginform');
		#$this->_forward('login');
    }

    public function loginAction()
    {
		if (null == $this->_form) {
		    $this->_form = $this->_getLoginForm();
		}

                //ak nebolo nic odoslane cez post tak sa vrat na index
		if (!$this->getRequest()->isPost()) {
		    #$this->view->form = $this->_form;
		    #return $this->render('loginform');
		    return $this->_forward('index');
		}

		if (!$this->_form->isValid($_POST) ||
                    !$this->_authUser()->isValid()) {
                    // Failed validation; redisplay form
		    $this->view->form = $this->_form;
                    $this->_forward('index');
		}
                else {
                    $this->_redirect('index');
                }

                
    }

    public function logoutAction() 
    {
		Zend_Auth::getInstance()->clearIdentity();
    }

    public function registerAction()
    {
	    if (null === $this->_form) {
		    $this->_form = $this->_getRegistrationForm();
		}
			
		if (!$this->getRequest()->isPost()) {
		    $this->view->form = $this->_form;
		    return $this->render('registerform');
		    #return $this->_forward('index');
		}
		
		
		if (!$this->_form->isValid($_POST)) {
		    // Failed validation; redisplay form
		    $this->view->form = $this->_form;
		    return $this->render('registerform');
		}
		
		$this->_registerNewUser();
		return $this->render('thx4reg');
    }

    public function thx4regAction() 
    {
    	
    }

    protected function _getLoginForm()
    {
		$form = new Zend_Form();
		$form->setAction($this->_helper->url('login'))
			->setMethod('post');
		// Create and configure username element:
		$username = $form->createElement('text', 'username');
		$username->setLabel('Login');
		$username->addValidator('alnum')
			->addValidator('regex', false, array('/^[a-z]+/'))
			->addValidator('stringLength', false, array(5, 20))
			->setRequired(true)
			->addFilter('StringToLower');
		// Create and configure password element:
		$password = $form->createElement('password', 'password');
		$password->setLabel('Heslo');
		$password->addValidator('StringLength', false, array(6))
			->setRequired(true);
		// Add elements to form:
		$form->addElement($username)
			->addElement($password)
			->addElement('submit', 'login', array('label' => 'Login'));
	        return $form;
    }

    protected function _authUser()
    {
		$dbAdapter = Zend_Registry::get('dbAdapter');
		
		$auth = Zend_Auth::getInstance();
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
		$authAdapter->setTableName('player')
			->setIdentityColumn('login')
			->setCredentialColumn('passwd');
		
		$authAdapter->setIdentity($this->_form->getValue('username'));
		$authAdapter->setCredential($this->_form->getValue('password'));
	
		$result = $auth->authenticate($authAdapter);
		
		if ($result->isValid()) {
                    $storage = $auth->getStorage();
                    $storage->write($authAdapter->getResultRowObject(null, 'passwd'));
                }
		
		return $result;
    }

    protected function _getRegistrationForm()
    {
		$form = new Zend_Form();
		$form->setAction($this->_helper->url('register'))
			->setMethod('post');
		
		// Create and configure username element:
		$username = $form->createElement('text', 'login');
		$username->setLabel('Login');
		$username->addValidator('alnum')
				->addValidator('regex', false, array('/^[a-z]+/'))
				->addValidator('stringLength', false, array(5, 20))
				->setRequired(true)
				->addFilter('StringToLower');
		
		// Create and configure password element:
		$password = $form->createElement('password', 'passwd');
		$password->setLabel('Heslo');
		$password->addValidator('StringLength', false, array(6))
				->setRequired(true);
		
		// Create and configure email element:
		$email = $form->createElement('text', 'email');
		$email->setLabel('Email');
		$email->setRequired(true)
			->addValidator('EmailAddress', false);
		
		// Create and configure phone number element:
		$gsm = $form->createElement('text', 'gsm');
		$gsm->setLabel('Tel.');
		$gsm->addValidator('alnum')
		->setRequired(false);
		
		// Add elements to form:
		$form->addElement($username)
			->addElement($password)
			->addElement($email)
			->addElement($gsm)
			->addElement('submit', 'register', array('label' => 'Register'));
		
		return $form;
    }

    protected function _registerNewUser()
    {
		#$this->render('thx4reg');
	    	//@todo overit ci dany uzivatel uz nahodou neexistuje
		//ulozit udaje uzivatela do db
		$table = $this->_getTable();
		$new_player_id = $table->insert($this->_form->getValues());
		
		$this->_model = new Player($new_player_id);
		//po uspesnom ulozeni mu to oznamit:
		// - mailom @todo oznamenie mailom
		// - vypisom na stranke
		#return $this->_helper->redirector('thx4reg');
    }

    protected function _getTable() 
    {
	    if (null === $this->_table) {
		    $this->_table = new Model_DbTable_Player();
		}
	
		return $this->_table;
    }
    
    protected function _getModel() 
    {
		if (null === $this->_model) {
			$this->_model = new Player($player_id);
		}
	
		return $this->_model;
    }
}


