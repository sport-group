<?php

class PlayerlistController extends Zend_Controller_Action
{
    protected $_model;

    public function indexAction()
    {
		//@todo dodelat vypis hracov
		$this->view->players = $this->_getModel();
    }

    protected function _getModel()
    {
		if (null === $this->_model) {
		    // @todo nacitanie suborov a tried prepracovat pomocou Zend_Loader
		    require_once APPLICATION_PATH.'/models/PlayerList.php';
		    $this->_model = new PlayerList();
		    $this->_model->fetchAllPlayers();
		}
		return $this->_model;
    }
}

